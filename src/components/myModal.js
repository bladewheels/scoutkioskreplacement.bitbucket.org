/** @jsx React.DOM */
import React from 'react';
import BootstrapModalMixin from './mixins/BootstrapModalMixin.js';

var MyModal = React.createClass({

        mixins: [BootstrapModalMixin /** See: http://bl.ocks.org/insin/raw/8449696/ **/],

        render: function() {
            var buttons = this.props.buttons.map(function(button) {
                return <button type="button" className={'btn btn-' + button.type} onClick={button.handler}>
                    {button.text}
                </button>
            });

            return (
                <div className="modal fade">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                {this.renderCloseButton()}
                                <strong>{this.props.header}</strong>
                            </div>
                            <div className="modal-body">
                                {this.props.children}
                            </div>
                            <div className="modal-footer">
                                {buttons}
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    });

module.exports = MyModal;
