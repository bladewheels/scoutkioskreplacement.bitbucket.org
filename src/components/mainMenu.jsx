import React from 'react';
import MyModal from './myModal.js';
import VouchersMenu from './vouchersMenu.jsx';

const MainMenu = React.createClass({

    getInitialState() {
        return { menuItemSelected: false };
    },

    handleMenuClick(item) {
        console.log('Main menu.handleMenuClick('+item+') called, setting state...');
        this.setState({menuItemSelected: true, menuItem: item});
    },

    render() {

        const styles = {
            menu: {
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: '80vw',
                marginLeft: '10vw'
            },
            menuButton: {
                marginBottom: 10
            }
        };

        const mainMenu = <div className="btn-group-vertical" role="group" style={styles.menu} >
                            <h3>Your Points: 1675</h3>
                            <button type="button" className="btn btn-primary" style={styles.menuButton} onClick={this.handleMenuClick.bind(this, 'Points')} >Points</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton} onClick={this.handleMenuClick.bind(this, 'FoodAndVouchers')} >Food &amp; Vouchers</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton}>Pure Rewards Playback</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton}>Special Offers &amp; Events</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton}>Draws</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton}>Merchandise</button>
                        </div>;

        if (this.state.menuItemSelected) {
            console.log('Main menu.render('+this.state.menuItem+') called...');
            switch (this.state.menuItem) {
                case 'FoodAndVouchers':
                    return <VouchersMenu/>;
                    break;
                case 'Points' :
                default:
                    return  <div>
                        {mainMenu}
                        {this.getMenuItemModalContent(this.state.menuItem)}
                    </div>;
            }
        } else {
            return mainMenu;
        }
    },

    handleExternalHide: function() {
        this.setState({menuItemSelected: false});
        this.refs.modal.hide();
    },

    getMenuItemModalContent: function(item) {

        const buttons = [{type: 'primary',  text: 'OK',  handler: this.handleExternalHide}];
        const style = {
            container: {
                border: '1px solid silver',
                marginTop: 10,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'lightsteelblue'
            }
        };
        let content;

        switch (this.state.menuItem) {
            case 'Points' :
                content =   <div>
                                <p>Thank you, $[name], for using your PURE Rewards points card!</p>
                                <p>Total visits here at $[casinoName]: $[locationVisits]</p>
                                <p>PURE Rewards points available: $[points]</p>
                                <p>Attention PURE Rewards Members. Members points have been updated to account for unaccounted redemptions. If you have a concern about your point totals please ask to see a history of your visits and redemptions.</p>
                            </div>;
                break;
            default:
                content =   <div>default</div>;
        }

        return  <MyModal ref="modal"
                            show={this.state.showOffers}
                            header={item}
                            buttons={buttons}
                            backdrop={'static'}
                >
                    <div style={style.container}>{content}</div>
                </MyModal>
    }
});
export default MainMenu;
