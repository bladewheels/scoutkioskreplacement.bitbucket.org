import React from 'react';
import MyModal from './myModal.js';

const VouchersMenu = React.createClass({

    getInitialState() {
        return { menuItemSelected: false };
    },

    handleMenuClick(item) {
        this.setState({menuItemSelected: true, menuItem: item});
    },

    render() {

        const styles = {
            menu: {
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
                alignItems: 'center',
                width: '80vw',
                marginLeft: '10vw'
            },
            menuButton: {
                marginBottom: 10
            }
        };

        const thisMenu = <div className="btn-group-vertical" role="group" style={styles.menu} >
                            <h3>Your Points: 1675</h3>
                            <button type="button" className="btn btn-primary" style={styles.menuButton} onClick={this.handleMenuClick.bind(this, 'tenPercentOff')} >10% off PURE Rewards voucher</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton} onClick={this.handleMenuClick.bind(this, 'fiveDollars')} >$5 Voucher, for 1,000 PURE Rewards points</button>
                            <button type="button" className="btn btn-primary" style={styles.menuButton} onClick={this.handleMenuClick.bind(this, 'tenDollars')} >$10 Voucher, for 2,000 PURE Rewards points</button>
                        </div>;

        if (this.state.menuItemSelected) {
            return  <div>
                {thisMenu}
                {this.getMenuItemModalContent(this.state.menuItem)}
            </div>;
        } else {
            return thisMenu;
        }
    },

    handleExternalHide: function() {
        this.setState({menuItemSelected: false});
        this.refs.modal.hide();
    },

    getMenuItemModalContent: function() {

        const buttons = [{type: 'primary',  text: 'Cancel',  handler: this.handleExternalHide}];
        const style = {
            container: {
                border: '1px solid silver',
                marginTop: 10,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: 'lightsteelblue'
            }
        };
        let instructions = <h5>Swipe to Redeem</h5>;
        let content = [];

        switch (this.state.menuItem) {
            case 'tenPercentOff' :
                content =   [instructions, <div><p style={{color: 'red'}}>(0) 10% off PURE Rewards voucher</p></div>];
                break;
            case 'fiveDollars' :
                content =   [instructions, <div><p style={{color: 'red'}}>(0) $5 voucher</p></div>];
                break;
            case 'tenDollars' :
                content =   [instructions, <div><p style={{color: 'red'}}>(0) $10 voucher</p></div>];
                break;
            default:
                content =   [<div>d'oh!</div>];
        }

        return  <MyModal ref="modal"
                            header={'Offers!'}
                            buttons={buttons}
                            backdrop={'static'}
                >
                    <div style={style.container}>{content}</div>
                </MyModal>
    }
});
export default VouchersMenu;
