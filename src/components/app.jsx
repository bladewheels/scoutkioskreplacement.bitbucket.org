import React from 'react';
import Welcome from './welcome.jsx'

const App = React.createClass({

    render(){
        return <Welcome location={'Edmonton'}/>
    }
});

React.render(<App/>, document.getElementById('app'));