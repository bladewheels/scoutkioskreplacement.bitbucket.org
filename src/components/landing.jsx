import React from 'react';
import MyModal from './myModal.js';
import TopNav from './topNav.jsx';
import MainMenu from './mainMenu.jsx';

const LandingPage = React.createClass({

    getInitialState() {
        return { showOffers: true };
    },

    componentDidMount() {
        if (this.state.showOffers) {
            this.refs.modal.show();
        }
    },

    render() {

            const styles = {
                outerContainer: {
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center'
                }
            };

            return (
                <div style={styles.outerContainer}>
                    <TopNav />
                    <MainMenu />
                    {this.getWelcomingOffersModalContent()}
                </div>
            );
    },

    handleExternalHide: function() {
        this.setState({menuItemSelected: false});
        this.refs.modal.hide();
    },

    getWelcomingOffersModalContent: function() {

        const buttons = [{type: 'primary',  text: 'OK',  handler: this.handleExternalHide}];
        const style = {
            container: {
                border: '1px solid silver',
                backgroundColor: 'palegreen',
                marginTop: 10
            }
        };

        return  <MyModal ref="modal"
                            show={this.state.showOffers}
                            header={"Offers!"}
                            buttons={buttons}
                            backdrop={'static'}
                >
                    <div style={style.container}>
                        <p>
                            {'() Reminders'}
                        </p>
                        <ul>
                            <li>Remember to print your 10% off voucher!</li>
                        </ul>
                    </div>
                    <div style={style.container}>
                        <p>
                            {'(50) Dailies!'}
                        </p>
                        <ul>
                            <li>Daily 50 points received for swiping in at Calgary, Edmonton or Yellowhead!</li>
                        </ul>
                    </div>

                    <div style={style.container}>
                        <p>
                            {'(100) Wednesday Senior Days'}
                        </p>
                        <ul>
                            <li>Wednesday Senior Days! Swipe in at our other locations for more points!</li>
                        </ul>
                    </div>
                </MyModal>
    }
});
export default LandingPage;
